package com.jamo.puzzle;

import net.jcip.annotations.Immutable;
import net.jcip.annotations.ThreadSafe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ThreadSafe
@Immutable
public class WordPuzzle {

    private final char[] chars;
    private final int numberOfRows;
    private final int numberOfCols;

    public WordPuzzle(String puzzle, int dimension) {
        int length = puzzle.length();
        if (length < 4) {
            throw new IllegalArgumentException("Content must not be empty or less then 4 symbols");
        }

        if (dimension < 2) {
            throw new IllegalArgumentException("Dimension number should be >= 2");
        }

        if ((length % dimension) != 0) {
            throw new IllegalArgumentException("Wrong numbersOfRows - we can not split puzzle to specified number of rows");
        }

        this.chars = puzzle.toCharArray();
        this.numberOfCols = dimension;
        this.numberOfRows = chars.length / dimension;
    }

    // Each entry of list is an array and means one found word in the puzzle. So each array consists of character's
    // positions in the original WordPuzzle content(which is string string)
    public List<int[]> find(String search) {
        if (search.isEmpty()) {
            throw new IllegalArgumentException("Search string must not be empty");
        }
        SearchContext context = new SearchContext(search);

        findLeftToRight(0, 0, context);
        findRightToLeft(0, numberOfCols - 1, context);
        findTopToBottom(0, 0, context);
        findBottomToTop(numberOfRows - 1, 0, context);

        return context.result;
    }

    private List<int[]> findLeftToRight(int row, int column, SearchContext context) {
        // We are out of existing rows. Entry point condition
        if (row >= numberOfRows) {
            return context.result;
        }

        // End of columns in the row - going to the next row
        if (column + context.searchLength > numberOfCols) {
            return findLeftToRight(row + 1, 0, context);
        }

        // Comparing
        int index = 0;
        int charIndex;
        for (int j = column; j < column + context.searchLength; ++j) {
            charIndex = getCharIndex(row, j);

            // Does not match - moving to the next column
            if (context.searchArray[index] != chars[charIndex]) {
                return findLeftToRight(row, column + 1, context);
            }

            // Match - save the result and go further
            context.buffer[index] = charIndex;
            ++index;
        }

        // Add result and move further
        context.appendBufferToResult();
        return findLeftToRight(row, column + context.searchLength, context);
    }

    private List<int[]> findRightToLeft(int row, int column, SearchContext context) {
        // We are out of existing rows. Entry point condition
        if (row >= numberOfRows) {
            return context.result;
        }

        // End of columns in the row - going to the next row
        if (column - context.searchLength < -1) {
            return findRightToLeft(row + 1, numberOfCols - 1, context);
        }

        // Comparing
        int index = 0;
        int charIndex;
        for (int j = column; j > column - context.searchLength; --j) {
            charIndex = getCharIndex(row, j);

            // Does not match - moving to the next column
            if (context.searchArray[index] != chars[charIndex]) {
                return findRightToLeft(row, column - 1, context);
            }

            // Match - save the result and go further
            context.buffer[index] = charIndex;
            ++index;
        }

        // Add result and move further
        context.appendBufferToResult();
        return findRightToLeft(row, column - context.searchLength, context);
    }

    private List<int[]> findTopToBottom(int row, int column, SearchContext context) {
        // Last column - exiting
        if (column >= numberOfCols) {
            return context.result;
        }

        // End of rows in the column - going to the next column
        if (row + context.searchLength > numberOfRows) {
            return findTopToBottom(0, column + 1, context);
        }

        // Comparing
        int index = 0;
        int charIndex;
        for (int j = row; j < row + context.searchLength; ++j) {
            charIndex = getCharIndex(j, column);

            // Does not match - moving to the next column
            if (context.searchArray[index] != chars[charIndex]) {
                return findTopToBottom(row + 1, column, context);
            }

            // Match - save the result and go further
            context.buffer[index] = charIndex;
            ++index;
        }

        // Add result and move further
        context.appendBufferToResult();
        return findTopToBottom(row + context.searchLength, column, context);
    }

    private List<int[]> findBottomToTop(int row, int column, SearchContext context) {
        // Last column - exiting
        if (column >= numberOfCols) {
            return context.result;
        }

        // End of rows in the column - going to the next column
        if (row - context.searchLength < -1) {
            return findBottomToTop(numberOfRows - 1, column + 1, context);
        }

        // Comparing
        int index = 0;
        int charIndex;
        for (int j = row; j > row - context.searchLength; --j) {
            charIndex = getCharIndex(j, column);

            // Does not match - moving to the next column
            if (context.searchArray[index] != chars[charIndex]) {
                return findBottomToTop(row - 1, column, context);
            }

            // Match - save the result and go further
            context.buffer[index] = charIndex;
            ++index;
        }

        // Add result and move further
        context.appendBufferToResult();
        return findBottomToTop(row - context.searchLength, column, context);
    }

    private int getCharIndex(int row, int column) {
        return (row * numberOfCols) + column;
    }

    @ThreadSafe
    @Immutable
    private static class SearchContext {
        private final int searchLength;
        private final char[] searchArray;
        private final List<int[]> result;
        private final int[] buffer;

        public SearchContext(String search) {
            this.searchLength = search.length();
            this.searchArray = search.toCharArray();
            this.result = new ArrayList<int[]>();
            this.buffer = new int[searchLength];
        }

        private void appendBufferToResult() {
            result.add(Arrays.copyOf(buffer, searchLength));
        }
    }
}
