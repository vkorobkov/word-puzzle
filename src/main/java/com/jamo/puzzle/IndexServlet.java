package com.jamo.puzzle;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

public class IndexServlet extends HttpServlet {

    public static final String SAMPLE_CONTENT = "VNYBKGSRORANGEETRNXWPLAEALKAPMHNWMRPOCAXBGATNOMEL";
    public static final String SAMPLE_WORD = "GRAPES";
    public static final String SAMPLE_DIMENSION = "7";

    public static final String FORM_CONTENT = "content";
    public static final String FORM_CONTENT_CHARS = "content_chars";
    public static final String FORM_DIMENSION = "dimension";
    public static final String FORM_WORD = "word";
    public static final String PARAM_HIGHLIGHT = "highlight";
    public static final String PARAM_FOUND_COUNT = "found_count";
    public static final String PARAM_ERROR = "error";
    public static final String ERR_DIMENSION_MUST_BE_A_NUMBER = "Dimension must be a number";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        render(request, response, SAMPLE_CONTENT, SAMPLE_DIMENSION, SAMPLE_WORD);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String content = StringUtils.trim(request.getParameter(FORM_CONTENT));
        String dimension = StringUtils.trim(request.getParameter(FORM_DIMENSION));
        String word = StringUtils.trim(request.getParameter(FORM_WORD));

        render(request, response, content, dimension, word);
    }

    private void render(HttpServletRequest request, HttpServletResponse response, String content, String dimension, String word) throws ServletException, IOException {
        // Putting here some useful presets
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(FORM_CONTENT, content);
        params.put(FORM_CONTENT_CHARS, content.toCharArray());
        params.put(FORM_DIMENSION, dimension);
        params.put(FORM_WORD, word);

        Set<String> positionsToHighLight = new HashSet<String>();
        List<int[]> wordsToHighLight;
        try {
            Integer intDimension = Integer.parseInt(dimension);
            wordsToHighLight = new WordPuzzle(content.toUpperCase(), intDimension).find(word.toUpperCase());
            for (int[] charsToHighLight : wordsToHighLight) {
                for (int charToLightLight: charsToHighLight) {
                    positionsToHighLight.add(String.valueOf(charToLightLight));
                }
            }
            params.put(PARAM_HIGHLIGHT, positionsToHighLight);
            params.put(PARAM_FOUND_COUNT, wordsToHighLight.size());
        }
        catch (NumberFormatException nfe) {
            params.put(PARAM_ERROR, ERR_DIMENSION_MUST_BE_A_NUMBER);
        }
        catch (IllegalArgumentException exception) {
            params.put(PARAM_ERROR, exception.getMessage());
        }

        // Rendering the page
        renderIndexJsp(request, response, params);
    }

    private void renderIndexJsp(HttpServletRequest request, HttpServletResponse response, Map<String, Object> attributes) throws ServletException, IOException {
        for (Map.Entry<String, Object> entry: attributes.entrySet()) {
            request.setAttribute(entry.getKey(), entry.getValue());
        }
        getServletConfig().getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
    }
}
