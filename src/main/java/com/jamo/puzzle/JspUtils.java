package com.jamo.puzzle;

import java.util.Set;

public class JspUtils {

    public static String ifContainsThenElse(Set<?> set, Object toCheck, String thenReturn, String elseReturn) {
        if (set == null) {
            return elseReturn;
        }
        return set.contains(String.valueOf(toCheck)) ? thenReturn : elseReturn;
    }
}
