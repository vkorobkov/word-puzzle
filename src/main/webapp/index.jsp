<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="jsputils" uri="/WEB-INF/tlds/jsputils" %>
<head>

    <style>
        .puzzle-dev {
            font-family: monospace;
            font-size: 30px;
        }

        .puzzle-value {
            display: inline-block;
        }

        .error {
            color: red;
            padding: 20px;
            border: solid 3px red;
            width: 400px;;
        }

        .highlight {
            background: yellow;
        }
    </style>
</head>
<body>
<h1>Hello this is a real WordPuzzle!</h1>

<h2>Setup your puzzle and guess the word here</h2>

<c:if test="${not empty error}">
    <div class="error"><strong>${error}</strong></div>
</c:if>

<form action="/" method="post">
    <label for="content">Content:</label><br/>
    <textarea name="content" id="content" cols="50" rows="4"><%=request.getAttribute("content")%></textarea>
    <br/><br/>

    <label for="dimension">Dimension:</label><br/>
    <input name="dimension" id="dimension" type="text" value="${dimension}"/><br/><br/>

    <label for="word">Word To Find:</label><br/>
    <input name="word" id="word" type="text" value="${word}"/><br/>
    <small>Also good words to find: banana, cherry, grapes, lemon, orange, tomato</small>

    <br/><br/>
    <button type="submit">Crack the puzzle!</button>
</form>

<c:if test="${empty error}">
    <h2>The puzzle itself</h2>
    <strong>Total found: ${found_count}</strong>
    <div class="puzzle-dev">
        <c:set var="count" value="0"/>
        <c:forEach items="${content_chars}" var="char">
            <div class="puzzle-value ${jsputils:ifContainsThenElse(highlight, count, 'highlight', '')}">
                    ${char}
            </div>

            <c:set var="count" value="${count + 1}"/>
            <c:if test="${count % dimension == 0}"><br/></c:if>
        </c:forEach>
    </div>
</c:if>

</body>